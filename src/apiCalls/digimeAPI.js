import fs from "fs";

export const diagnosis = function(diagnosis) {
    
    fs.readFile('./ConditionData2.json', 'utf8', function(err, contents) {
        var diagnosis = [];
        if (contents.includes("diagnosis")) {
            // "code": {"text": "Diabetes Type 1","coding":
            var json = JSON.parse(contents);
            for(var i=0; i< json.fileData.length; i++) {
                diagnosis.push(json.fileData[i].code.text);
            }
            
            console.log(diagnosis);
        }
    });
    return diagnosis;
}

export const medications = function(medications) {
    
    fs.readFile('./MedicationData.json', 'utf8', function(err, contents) {
        var medications = [];
        if (contents.includes("medicationreference")) {
            // "code": {"text": "Diabetes Type 1","coding":
            var json = JSON.parse(contents);
            
            for(var i=0; i< json.fileData.length; i++) {
                var string = json.fileData[i].medicationreference.code.text + " " + 
                    json.fileData[i].dosageinstruction[0].dosequantity.value + 
                    json.fileData[i].dosageinstruction[0].dosequantity.unit + 
                    " " + json.fileData[i].dosageinstruction[0].text; 
                medications.push(string);
            }
            
            console.log(medications);
        }
    });
    return medications;
}


diagnosis();
medications();
