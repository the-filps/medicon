export const conditions= {"fileData":
	[{"accountentityid":"16_3003765099",
	"asserteddate":1499644800000,"date":1499644800000,
	"asserter":{
    	"display": "moore, sean",
    	"reference": "https://open-ic.epic.com/fhir/api/fhir/dstu2/practitioner/titwfhjchtlo0pfh9nzctsqb"
	},
	"baseid": "1234",
	"category": [{
        "text": "diagnosis",
        "coding": [{
			"system": "http://loinc.org",
            "code": "29308-4",
            "display": "diagnosis"
		}, {
            "system": "http://snomed.info/sct",
            "code": "439401001",
            "display": "diagnosis"
		}, {
			"system": "http://hl7.org/fhir/condition-category",
            "code": "diagnosis",
            "display": "diagnosis"
		}, {
			"system": "http://argonautwiki.hl7.org/extension-codes",
            "code": "problem",
            "display": "problem"
		}]
    }],
	"clinicalstatus": "alive",
	"code": {
		"text": "High colesterol",
    	"coding": [ {
			"system": "urn:oid:2.16.840.1.113883.6.90",
            "code": "w54.0xxa",
            "display": "High colesterol"
        }, {
            "system": "http://snomed.info/sct",
            "code": "283734005",
            "display": "High colesterol"
        }]},
	"createddate":1499644800000,
	"entityid": "20_tukrxl29bxe9lyacdtiyrwc6ln5gz-z7clr2r-2sy964b_tt4fnwwh8zm2zgwsbocyvuab",
	"id": "tt4fnwwh8zm2zgwsbocyvuab",
	"identifier": [12345],
	"onsetdatetime":1499644800000,
	"referenceentityid": "referenceentityid",
	"referenceentitytype": 12345,
	"severity": {
    	"text": "medium",
    	"coding": [{
			"system": "urn:oid:2.16.840.1.113883.6.90",
            "code": "w54.0xxa",
            "display": "High colesterol"
        }, {
			"system": "http://snomed.info/sct",
            "code": "283734005",
            "display": "High colesterol"
        }]},
	"socialnetworkuserentityid":"socialnetworkuserentityid",
	"updateddate":1499644800000,
	"verificationstatus":"true",

	"fileName":"18_2_16_10_129_D201707_1.json",
	"fileList":[
		"18_1_4_2_7_D201709_1.json",
		"18_2_16_10_129_D201804_1.json",
		"18_2_16_10_130_D201710_1.json",
		"18_2_16_10_130_D201801_1.json",
		"18_2_16_10_132_D201709_1.json",
		"18_2_16_10_136_D201802_1.json",
		"18_2_16_10_136_D201805_1.json",
		"18_2_16_10_136_D201806_1.json",
		"18_1_4_2_2_D201709_1.json",
		"18_1_4_2_2_D201710_1.json",
		"18_2_16_10_129_D201707_1.json",
		"18_2_16_10_130_D201711_1.json",
		"18_2_16_10_130_D201804_1.json",
		"18_2_16_10_132_D201801_1.json",
		"18_2_16_10_136_D201710_1.json",
		"18_1_4_2_1_D201709_1.json",
		"18_2_16_10_132_D201712_1.json",
		"18_1_4_2_1_D201710_1.json",
		"18_5_19_6_406_D201805_1.json",
		"18_2_16_10_132_D201802_1.json",
		"18_1_4_2_7_D201708_1.json",
		"18_5_19_6_405_D201803_1.json",
		"18_5_19_6_405_D201805_1.json",
		"18_2_16_10_130_D201806_1.json",
		"18_2_16_10_136_D201709_1.json",
		"18_1_4_2_2_D201804_1.json",
		"18_2_16_10_130_D201803_1.json",
		"18_1_9_1_2_D201804_1.json",
		"18_4_18_3_301_D201804_1.json",
		"18_1_4_2_1_D201804_1.json",
		"18_1_4_2_2_D201801_1.json",
		"18_1_9_1_1_D201804_1.json",
		"18_1_9_1_2_D201709_1.json",
		"18_1_4_2_1_D201801_1.json",
		"18_4_18_3_303_D201804_1.json",
		"18_4_18_3_300_D201804_1.json",
		"18_1_9_1_2_D201707_1.json",
		"18_4_18_3_300_D201711_1.json",
		"18_4_18_3_301_D201711_1.json",
		"18_1_9_1_2_D201711_1.json",
		"18_4_18_3_301_D201802_1.json",
		"18_4_18_3_301_D201712_1.json",
		"18_4_18_3_301_D201801_1.json",
		"18_4_18_3_301_D201803_1.json",
		"18_4_18_3_300_D201712_1.json",
		"18_1_9_1_1_D201709_1.json",
		"18_4_18_3_300_D201802_1.json",
		"18_4_18_3_303_D201711_1.json",
		"18_1_9_1_1_D201707_1.json",
		"18_4_18_3_300_D201803_1.json",
		"18_4_18_3_303_D201802_1.json",
		"18_5_19_6_406_D201712_1.json",
		"18_4_18_3_300_D201801_1.json",
		"18_1_9_1_1_D201711_1.json",
		"18_4_18_3_303_D201801_1.json",
		"18_4_18_3_303_D201803_1.json",
		"18_5_19_6_406_D201806_1.json",
		"18_4_18_3_303_D201712_1.json",
		"18_1_3_7_1_D201806_1.json",
		"18_1_3_7_2_D201806_1.json",
		"18_5_19_6_406_D201801_1.json",
		"18_1_3_7_1_D201804_1.json",
		"18_1_3_7_2_D201804_1.json",
		"18_1_3_7_2_D201801_1.json",
		"18_1_3_7_2_D201707_1.json",
		"18_1_3_7_1_D201801_1.json",
		"18_1_3_7_2_D201712_1.json",
		"18_1_3_7_1_D201802_1.json",
		"18_1_3_7_2_D201706_1.json",
		"18_1_3_7_1_D201712_1.json",
		"18_1_3_7_1_D201803_1.json",
		"18_1_3_7_1_D201707_1.json",
		"18_1_3_7_2_D201709_1.json",
		"18_1_3_7_2_D201711_1.json",
		"18_1_3_7_1_D201711_1.json",
		"18_1_3_7_1_D201709_1.json",
		"18_1_3_7_1_D201706_1.json",
		"18_1_3_7_1_D201708_1.json",
		"18_1_3_7_2_D201710_1.json",
		"18_1_3_7_1_D201710_1.json",
		"18_1_3_7_2_D201802_1.json",
		"18_1_3_7_2_D201803_1.json",
		"18_1_3_7_2_D201708_1.json",
		"18_5_19_6_404_D201712_1.json",
		"18_5_19_6_405_D201712_1.json"]
}, 
{"accountentityid":"16_3003765099",
"asserteddate":1499644800000,"date":1499644800000,
"asserter":{
    "display": "moore, sean",
    "reference": "https://open-ic.epic.com/fhir/api/fhir/dstu2/practitioner/titwfhjchtlo0pfh9nzctsqb"
},
"baseid": "1234",
"category": [
    {
        "text": "diagnosis",
        "coding": [
            {
                "system": "http://loinc.org",
                "code": "29308-4",
                "display": "diagnosis"
            },
            {
                "system": "http://snomed.info/sct",
                "code": "439401001",
                "display": "diagnosis"
            },
            {
                "system": "http://hl7.org/fhir/condition-category",
                "code": "diagnosis",
                "display": "diagnosis"
            },
            {
                "system": "http://argonautwiki.hl7.org/extension-codes",
                "code": "problem",
                "display": "problem"
            }
        ]
    }
],
"clinicalstatus": "alive",
"code": {
    "text": "Diabetes Type 1",
    "coding": [
        {
            "system": "urn:oid:2.16.840.1.113883.6.90",
            "code": "w54.0xxa",
            "display": "Diabetes Type 1"
        },
        {
            "system": "http://snomed.info/sct",
            "code": "283734005",
            "display": "Diabetes Type 1"
        }
    ]
},
"createddate":1499644800000,
"entityid": "20_tukrxl29bxe9lyacdtiyrwc6ln5gz-z7clr2r-2sy964b_tt4fnwwh8zm2zgwsbocyvuab",
"id": "tt4fnwwh8zm2zgwsbocyvuab",
"identifier": [12345],
"onsetdatetime":1499644800000,
"referenceentityid": "referenceentityid",
"referenceentitytype": 12345,
"severity": {
    "text": "medium",
    "coding": [
        {
            "system": "urn:oid:2.16.840.1.113883.6.90",
            "code": "w54.0xxa",
            "display": "Diabetes Type 1"
        },
        {
            "system": "http://snomed.info/sct",
            "code": "283734005",
            "display": "Diabetes Type 1"
        }
    ]
},
"socialnetworkuserentityid":"socialnetworkuserentityid",
"updateddate":1499644800000,
	"verificationstatus":"true",

	"fileName":"18_2_16_10_129_D201707_1.json",
	"fileList":[["18_1_4_2_7_D201709_1.json","18_2_16_10_129_D201804_1.json","18_2_16_10_130_D201710_1.json","18_2_16_10_130_D201801_1.json","18_2_16_10_132_D201709_1.json","18_2_16_10_136_D201802_1.json","18_2_16_10_136_D201805_1.json","18_2_16_10_136_D201806_1.json","18_1_4_2_2_D201709_1.json","18_1_4_2_2_D201710_1.json","18_2_16_10_129_D201707_1.json","18_2_16_10_130_D201711_1.json","18_2_16_10_130_D201804_1.json","18_2_16_10_132_D201801_1.json","18_2_16_10_136_D201710_1.json","18_1_4_2_1_D201709_1.json","18_2_16_10_132_D201712_1.json","18_1_4_2_1_D201710_1.json","18_5_19_6_406_D201805_1.json","18_2_16_10_132_D201802_1.json","18_1_4_2_7_D201708_1.json","18_5_19_6_405_D201803_1.json","18_5_19_6_405_D201805_1.json","18_2_16_10_130_D201806_1.json","18_2_16_10_136_D201709_1.json","18_1_4_2_2_D201804_1.json","18_2_16_10_130_D201803_1.json","18_1_9_1_2_D201804_1.json","18_4_18_3_301_D201804_1.json","18_1_4_2_1_D201804_1.json","18_1_4_2_2_D201801_1.json","18_1_9_1_1_D201804_1.json","18_1_9_1_2_D201709_1.json","18_1_4_2_1_D201801_1.json","18_4_18_3_303_D201804_1.json","18_4_18_3_300_D201804_1.json","18_1_9_1_2_D201707_1.json","18_4_18_3_300_D201711_1.json","18_4_18_3_301_D201711_1.json","18_1_9_1_2_D201711_1.json","18_4_18_3_301_D201802_1.json","18_4_18_3_301_D201712_1.json","18_4_18_3_301_D201801_1.json","18_4_18_3_301_D201803_1.json","18_4_18_3_300_D201712_1.json","18_1_9_1_1_D201709_1.json","18_4_18_3_300_D201802_1.json","18_4_18_3_303_D201711_1.json","18_1_9_1_1_D201707_1.json","18_4_18_3_300_D201803_1.json","18_4_18_3_303_D201802_1.json","18_5_19_6_406_D201712_1.json","18_4_18_3_300_D201801_1.json","18_1_9_1_1_D201711_1.json","18_4_18_3_303_D201801_1.json","18_4_18_3_303_D201803_1.json","18_5_19_6_406_D201806_1.json","18_4_18_3_303_D201712_1.json","18_1_3_7_1_D201806_1.json","18_1_3_7_2_D201806_1.json","18_5_19_6_406_D201801_1.json","18_1_3_7_1_D201804_1.json","18_1_3_7_2_D201804_1.json","18_1_3_7_2_D201801_1.json","18_1_3_7_2_D201707_1.json","18_1_3_7_1_D201801_1.json","18_1_3_7_2_D201712_1.json","18_1_3_7_1_D201802_1.json","18_1_3_7_2_D201706_1.json","18_1_3_7_1_D201712_1.json","18_1_3_7_1_D201803_1.json","18_1_3_7_1_D201707_1.json","18_1_3_7_2_D201709_1.json","18_1_3_7_2_D201711_1.json","18_1_3_7_1_D201711_1.json","18_1_3_7_1_D201709_1.json","18_1_3_7_1_D201706_1.json","18_1_3_7_1_D201708_1.json","18_1_3_7_2_D201710_1.json","18_1_3_7_1_D201710_1.json","18_1_3_7_2_D201802_1.json","18_1_3_7_2_D201803_1.json","18_1_3_7_2_D201708_1.json","18_5_19_6_404_D201712_1.json","18_5_19_6_405_D201712_1.json"]
	]
}, 
{"accountentityid":"16_3003765099",
"asserteddate":1499644800000,"date":1499644800000,
"asserter":{
    "display": "moore, sean",
    "reference": "https://open-ic.epic.com/fhir/api/fhir/dstu2/practitioner/titwfhjchtlo0pfh9nzctsqb"
},
"baseid": "1234",
"category": [
    {
        "text": "diagnosis",
        "coding": [
            {
                "system": "http://loinc.org",
                "code": "29308-4",
                "display": "diagnosis"
            },
            {
                "system": "http://snomed.info/sct",
                "code": "439401001",
                "display": "diagnosis"
            },
            {
                "system": "http://hl7.org/fhir/condition-category",
                "code": "diagnosis",
                "display": "diagnosis"
            },
            {
                "system": "http://argonautwiki.hl7.org/extension-codes",
                "code": "problem",
                "display": "problem"
            }
        ]
    }
],
"clinicalstatus": "alive",
"code": {
    "text": "High blood pressure",
    "coding": [
        {
            "system": "urn:oid:2.16.840.1.113883.6.90",
            "code": "w54.0xxa",
            "display": "High blood pressure"
        },
        {
            "system": "http://snomed.info/sct",
            "code": "283734005",
            "display": "High blood pressure1"
        }
    ]
},
"createddate":1499644800000,
"entityid": "20_tukrxl29bxe9lyacdtiyrwc6ln5gz-z7clr2r-2sy964b_tt4fnwwh8zm2zgwsbocyvuab",
"id": "tt4fnwwh8zm2zgwsbocyvuab",
"identifier": [12345],
"onsetdatetime":1499644800000,
"referenceentityid": "referenceentityid",
"referenceentitytype": 12345,
"severity": {
    "text": "medium",
    "coding": [
        {
            "system": "urn:oid:2.16.840.1.113883.6.90",
            "code": "w54.0xxa",
            "display": "High blood pressure"
        },
        {
            "system": "http://snomed.info/sct",
            "code": "283734005",
            "display": "High blood pressure"
        }
    ]
},
"socialnetworkuserentityid":"socialnetworkuserentityid",
"updateddate":1499644800000,
"verificationstatus":"true",

	"fileName":"18_2_16_10_129_D201707_1.json",
	"fileList":[
		"18_1_4_2_7_D201709_1.json",
		"18_2_16_10_129_D201804_1.json",
		"18_2_16_10_130_D201710_1.json",
		"18_2_16_10_130_D201801_1.json",
		"18_2_16_10_132_D201709_1.json",
		"18_2_16_10_136_D201802_1.json",
		"18_2_16_10_136_D201805_1.json",
		"18_2_16_10_136_D201806_1.json",
		"18_1_4_2_2_D201709_1.json",
		"18_1_4_2_2_D201710_1.json",
		"18_2_16_10_129_D201707_1.json",
		"18_2_16_10_130_D201711_1.json",
		"18_2_16_10_130_D201804_1.json",
		"18_2_16_10_132_D201801_1.json",
		"18_2_16_10_136_D201710_1.json",
		"18_1_4_2_1_D201709_1.json",
		"18_2_16_10_132_D201712_1.json",
		"18_1_4_2_1_D201710_1.json",
		"18_5_19_6_406_D201805_1.json",
		"18_2_16_10_132_D201802_1.json",
		"18_1_4_2_7_D201708_1.json",
		"18_5_19_6_405_D201803_1.json",
		"18_5_19_6_405_D201805_1.json",
		"18_2_16_10_130_D201806_1.json",
		"18_2_16_10_136_D201709_1.json",
		"18_1_4_2_2_D201804_1.json",
		"18_2_16_10_130_D201803_1.json",
		"18_1_9_1_2_D201804_1.json",
		"18_4_18_3_301_D201804_1.json",
		"18_1_4_2_1_D201804_1.json",
		"18_1_4_2_2_D201801_1.json",
		"18_1_9_1_1_D201804_1.json",
		"18_1_9_1_2_D201709_1.json",
		"18_1_4_2_1_D201801_1.json",
		"18_4_18_3_303_D201804_1.json",
		"18_4_18_3_300_D201804_1.json",
		"18_1_9_1_2_D201707_1.json",
		"18_4_18_3_300_D201711_1.json",
		"18_4_18_3_301_D201711_1.json",
		"18_1_9_1_2_D201711_1.json",
		"18_4_18_3_301_D201802_1.json",
		"18_4_18_3_301_D201712_1.json",
		"18_4_18_3_301_D201801_1.json",
		"18_4_18_3_301_D201803_1.json",
		"18_4_18_3_300_D201712_1.json",
		"18_1_9_1_1_D201709_1.json",
		"18_4_18_3_300_D201802_1.json",
		"18_4_18_3_303_D201711_1.json",
		"18_1_9_1_1_D201707_1.json",
		"18_4_18_3_300_D201803_1.json",
		"18_4_18_3_303_D201802_1.json",
		"18_5_19_6_406_D201712_1.json",
		"18_4_18_3_300_D201801_1.json",
		"18_1_9_1_1_D201711_1.json",
		"18_4_18_3_303_D201801_1.json",
		"18_4_18_3_303_D201803_1.json",
		"18_5_19_6_406_D201806_1.json",
		"18_4_18_3_303_D201712_1.json",
		"18_1_3_7_1_D201806_1.json",
		"18_1_3_7_2_D201806_1.json",
		"18_5_19_6_406_D201801_1.json",
		"18_1_3_7_1_D201804_1.json",
		"18_1_3_7_2_D201804_1.json",
		"18_1_3_7_2_D201801_1.json",
		"18_1_3_7_2_D201707_1.json",
		"18_1_3_7_1_D201801_1.json",
		"18_1_3_7_2_D201712_1.json",
		"18_1_3_7_1_D201802_1.json",
		"18_1_3_7_2_D201706_1.json",
		"18_1_3_7_1_D201712_1.json",
		"18_1_3_7_1_D201803_1.json",
		"18_1_3_7_1_D201707_1.json",
		"18_1_3_7_2_D201709_1.json",
		"18_1_3_7_2_D201711_1.json",
		"18_1_3_7_1_D201711_1.json",
		"18_1_3_7_1_D201709_1.json",
		"18_1_3_7_1_D201706_1.json",
		"18_1_3_7_1_D201708_1.json",
		"18_1_3_7_2_D201710_1.json",
		"18_1_3_7_1_D201710_1.json",
		"18_1_3_7_2_D201802_1.json",
		"18_1_3_7_2_D201803_1.json",
		"18_1_3_7_2_D201708_1.json",
		"18_5_19_6_404_D201712_1.json",
		"18_5_19_6_405_D201712_1.json"]
}]}