export const data = {
	"person": {
		"doctor": {
			"key" : 0,
			"name": "doctor",
			"n": 0,
			"children": ["needs"],
			"parents": []
		},
		"patient": {
			"key" : 1,
			"name": "patient",
			"n": 0,
			"children": ["needs"],
			"parents": []
		}
	},
	"needs": {
		"physical": {
			"key" : 2,
			"name": "physical",
			"n": 0,
			"children": ["request"],
			"parents": ["person"]
		},
		"mental": {
			"key" : 3,
			"name": "mental",
			"n": 0,
			"children": [],
			"parents": ["person"]
		},
		"emotional": {
			"key" : 4,
			"name": "emotional",
			"n": 0,
			"children": ["request"],
			"parents": ["person"]
		}
	},
	"request": {
		"discomfort": {
			"key" : 5,
			"name": "discomfort",
			"n": 0,
			"children": ["discomfortType"],
			"parents": ["needs"]
		},
		"medicine": {
			"key" : 6,
			"name": "medicine",
			"n": 0,
			"children": ["discomfortType"],
			"parents": ["needs"]
		},
		"test": {
			"key" : 7,
			"name": "test",
			"n": 0,
			"children": ["discomfortType"],
			"parents": ["needs"]
		}
	},
	"discomfortType": {
		"pain": {
			"key" : 8,
			"name": "pain",
			"n": 0,
			"children": ["wholeBody"],
			"parents": ["request"]
		},
		"dizziness": {
			"key" : 9,
			"name": "dizziness",
			"n": 0,
			"children": ["wholeBody"],
			"parents": ["request"]
		},
		"itchiness": {
			"key" : 10,
			"name": "itchiness",
			"n": 0,
			"children": ["wholeBody"],
			"parents": ["request"]
		}
	},
	"quantity": {
		"scale": {
			"key" : 11,
			"name": "scale",
			"n": 0,
			"children": [""],
			"parents": ["wholeBody"]
		}
	},
	"wholeBody": {
		"head": {
			"key" : 21,
			"name": "head",
			"n": 0,
			"children": ["headDetail"],
			"parents": ["discomfortType"]
		},
		"torso": {
			"key" : 22,
			"name": "torso",
			"n": 0,
			"children": ["torsoDetail"],
			"parents": ["discomfortType"]
		},
		"legs": {
			"key" : 23,
			"name": "legs",
			"n": 0,
			"children": ["legsDetail"],
			"parents": ["discomfortType"]
		}
	},
	
	"torsoDetail": {
		"upperTorso": {
			"key" : 24,
			"name": "upperTorso",
			"n": 0,
			"children": ["quantity"],
			"parents": ["wholeBody"]
		},
		"midTorso": {
			"key" : 25,
			"name": "midTorso",
			"n": 0,
			"children": ["midTorso"],
			"parents": ["wholeBody"]
		},
		"lowerTorso": {
			"key" : 26,
			"name": "lowerTorso",
			"n": 0,
			"children": ["quantity"],
			"parents": ["wholeBody"]
		}
	},
	"midTorso": {
		"leftBreast": {
			"key" : 27,
			"name": "leftBreast",
			"n": 0,
			"area": {left: 0, top: 10, size: 40},
			"children": ["quantity"],
			"parents": ["torsoDetail"]
		},
		"rightBreast": {
			"key" : 28,
			"name": "rightBreast",
			"n": 0,
			"area": {left: 0, top: 10, size: 40},
			"children": ["quantity"],
			"parents": ["torsoDetail"]
		},
		"chest": {
			"key" : 29,
			"name": "chest",
			"n": 0,
			"area": {left: 0, top: 10, size: 40},
			"children": ["quantity"],
			"parents": ["torsoDetail"]
		},
		"abdomen": {
			"key" : 30,
			"name": "abdomen",
			"n": 0,
			"area": {left: 0, top: 10, size: 40},
			"children": ["quantity"],
			"parents": ["torsoDetail"]
		},
	}
}
