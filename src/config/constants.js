export const ICON_PATH = 'images/icons';
export const CLOSE_ICON = `${ICON_PATH}/close.svg`;
export const ATTENTION_ICON = `${ICON_PATH}/attention.svg`;
export const DOWN_ICON = `${ICON_PATH}/down.svg`;
export const INFO_ICON = `${ICON_PATH}/info.svg`;
export const SEND_ICON = `${ICON_PATH}/send.svg`;
export const BACK_ICON = `${ICON_PATH}/back.svg`;
