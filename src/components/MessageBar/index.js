import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Node } from '../tree';
import './index.scss';
import NavButton from '../NavButton/index';
import { SEND_ICON } from '../../config/constants';


const propTypes = {
  items: PropTypes.arrayOf(PropTypes.string),
  onSubmit: PropTypes.func,
};

const defaultProps = {
  items: [],
  onSubmit: () => { },
};

export default class MessageBar extends Component {
  render() {
    const items = this.props.items.map((item) => ( <Node key={item} name={item} size={60} /> ));
    return (
      <div className='MessageBar'>
        <div className='MessageBar__container'>
          {items}
        </div>
        <NavButton name='send' icon={SEND_ICON} onClick={this.props.onSubmit} />
      </div>
    );
  }
}

MessageBar.propTypes = propTypes;
MessageBar.defaultProps = defaultProps;
