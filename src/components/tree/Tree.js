import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Node from './Node';
import './tree.scss';

const propTypes = {
	currentChildren: PropTypes.object,
	onClick: PropTypes.func,
	currentState: PropTypes.string,
}

const defaultProps = {
	currentChildren: null,
	onClick: () => { },
	currentState: ''
}

export default class Tree extends Component {
	makeNodes = () => {
		const keys = Object.keys(this.props.currentChildren);
		const nodes = keys.map((k) => {
			return (
				<Node
					key={k}
					size={40}
					onClick={this.props.onClick}
					treeState={this.props.currentState}
					name={k}
				/>
			)
		});
		return nodes;
	}
  render() {
		const renderNodes = this.makeNodes();
    return (
      <div className="Tree">
				{renderNodes}
				{/* <MessageBar/> */}
      </div>
    );
  }
}

Tree.propTypes = propTypes;
Tree.defaultProps = defaultProps;
