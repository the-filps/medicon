import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './tree.scss';

const propTypes = {
	size: PropTypes.number,
	onClick: PropTypes.func,
	name: PropTypes.string,
	treeState: PropTypes.string,
}

const defaultProps = {
	size: 40,
	onClick: () => { },
	name: '',
	treeState: 'person',
}

export default class Node extends Component {
  render() {
		const {name, treeState, onClick} = {...this.props};
    return (
			<div
				className={`Node ${name}`}
				onClick={() => onClick(name, treeState)}
			>
				<img
					src={`images/${name}.svg`}
					alt={name}
					width={`${this.props.size}%`}
				/>
      </div>
    );
  }
}

Node.propTypes = propTypes;
Node.defaultProps = defaultProps;
