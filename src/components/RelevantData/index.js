import React, { Component } from 'react';
import { conditions } from '../../apiCalls/ConditionData2';
import { medications } from '../../apiCalls/MedicationData';
import './index.scss';

export default class RelevantData extends Component {
    render() {
        var diagnosis = [];
        var prescriptions = [];

        for (var i = 0; i < conditions.fileData.length; i++) {
            diagnosis.push(conditions.fileData[i].code.text);
        }
        for (i = 0; i < medications.fileData.length; i++) {
            var string = medications.fileData[i].medicationreference.code.text + " " +
                medications.fileData[i].dosageinstruction[0].dosequantity.value +
                medications.fileData[i].dosageinstruction[0].dosequantity.unit +
                " " + medications.fileData[i].dosageinstruction[0].text;
            prescriptions.push(string);

        }
        var listDiagnosis = diagnosis.map((diagnosis) => <li>{diagnosis}</li>);
        var listMedications = prescriptions.map((medications) => <li>{medications}</li>);


        console.log(diagnosis);

        console.log(prescriptions);

        return (
            <div className='RelevantData'>
                <div className='RelevantData__diagnosis'>
                <span>Conditions:</span>
                    <ul>{listDiagnosis}</ul>
                </div>
                <div className='RelevantData__prescriptions'>
                <span>Prescriptions:</span>
                    <ul>{listMedications}</ul>
                </div>
                <div className='RelevantData__family'>
                <span>Family History:</span>
                    <ul></ul>
                </div>
            </div>
        );
    }
}
