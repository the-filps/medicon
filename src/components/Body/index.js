import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './index.scss';

const BODY_IMG_URL = 'images/';

const PART_MAPPING = {
  torsoDetail: {
    images: [
      {
        src: `${BODY_IMG_URL}torso.svg`,
        alt: 'Torso',
        className: 'torso',
        child: 'midTorso'
      }
    ]
  },
  midTorso: {
    images: [
      {
        src: `${BODY_IMG_URL}midTorso.svg`,
        alt: 'Mid torso',
        className: 'midTorso',
        child: 'chest'
      }
    ]
  },
  wholeBody: {
    images: [
      {
        src: `${BODY_IMG_URL}body_front.svg`,
        alt: 'Body front',
        className: 'wholeBody',
        child: 'torso'
      },
      {
        src: `${BODY_IMG_URL}body_back.svg`,
        alt: 'Body back',
        className: 'wholeBody',
        child: 'torso'
      }
    ]
  }
}


const propTypes = {
  part: PropTypes.string,
  onClick: PropTypes.func,
};

const defaultProps = {
  part: 'wholeBody',
  onClick: () => {}
};

export default class BodySelect extends Component {
  render() {
    const {part, onClick} = {...this.props};
    const { images } = { ...PART_MAPPING[part] };
    const imageComps = images.map((props) => {
      const { src, alt, className, child } = { ...props };
      return (
      <img
        alt={alt}
        src={src}
        className={className}
        onClick={() => onClick(child, part)}
      />);
    });
    //const areaComps = areas.map((area) => <div className='BodyPart-area' ></div>);
    return (
      <div className='BodySelect'>
        {imageComps}
      </div>
    )
  }
}

BodySelect.propTypes = propTypes;
BodySelect.defaultProps = defaultProps;
