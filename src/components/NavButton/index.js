import React from 'react';
import { PropTypes } from 'prop-types';
import { CLOSE_ICON, ATTENTION_ICON } from '../../config/constants';
import './indec.scss';


const propTypes = {
  icon: PropTypes.string,
  name: PropTypes.string,
  size: PropTypes.number,
  attention: PropTypes.bool,
  onClick: PropTypes.func,
};

const defaultProps = {
  icon: CLOSE_ICON,
  name: 'close',
  size: 40,
  attention: false,
  onClick: () => { },
}

const NavButton = (props) => (
  <div className={`NavButton ${props.name}`} onClick={props.onClick}>
    <img src={props.icon} alt={props.name} height={`${props.size}px`}/>
    {props.attention && <img src={ATTENTION_ICON} alt='Attention' className='NavButton__attention'/>}
  </div>
)

NavButton.propTypes = propTypes;
NavButton.defaultProps = defaultProps;
export default NavButton;
