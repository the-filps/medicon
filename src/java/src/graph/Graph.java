package graph;

import java.util.ArrayList;
import java.util.List;

public class Graph {
	
	List<Node> nodes;
	
	public Graph() {
		nodes = new ArrayList<>();
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	
	
}
