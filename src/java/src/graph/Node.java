package graph;

import java.util.List;

public class Node {
	
	private long key;

	private String name;
	
	// How many times the node has been visited
	private long n;
	
	private List<Node> children;
	
	private List<Node> parents;
	
	public Node() {
		children = null;
		parents = null;
		n = 0;
		name = "None";
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getN() {
		return n;
	}

	public void setN(long n) {
		this.n = n;
	}

	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public List<Node> getParents() {
		return parents;
	}

	public void setParents(List<Node> parents) {
		this.parents = parents;
	}


	public long getKey() {
		return key;
	}


	public void setKey(long key) {
		this.key = key;
	}
	

}
