import React, { Component } from 'react';
import { Node } from './components/tree';
import { data } from './data.js';
import './App.scss';
import MessageBar from './components/MessageBar/index';
import BodySelect from './components/Body/index';
import Slider from './components/CircleSlider/index';
import NavButton from './components/NavButton/index';
import { CLOSE_ICON, INFO_ICON, BACK_ICON } from './config/constants';
import RelevantData from './components/RelevantData/index';


const COMPONENT_MAPPING = {
  person: {component: (props) => (<Node size={60} {...props}/>), type: 'node'},
  needs: {component: (props) => (<Node {...props}/>), type: 'node'},
  request: {component: (props) => (<Node {...props}/>), type: 'node'},
  discomfortType: {component: (props) => (<Node {...props}/>), type: 'node'},
  quantity: {component: (props) => (<input {...props}></input>), type: 'quantity'},
  wholeBody: {component: (props) => (<BodySelect {...props} />), type: 'body'},
  torsoDetail: {component: (props) => (<BodySelect {...props}/>), type: 'body'},
  midTorso: {component: (props) => (<BodySelect {...props}/>), type: 'body'},
}

const initialState = {
  isFinish: false,
  messages: [],
  fullTree: data,
  showMessage: false,
  currentStateKey: 'person',
  currentState: data['person'],
  nextState: null,
  relevantData: false,
  nextChildren: ['doctor', 'patient'],
  showInfo: false,
};

class App extends Component {
  state = initialState;

  onNodeClick = (name, treeState) => {
    const {messages, fullTree} = {...this.state};

    const newMessages = messages;
    if (this.state.currentStateKey !== 'person') {
      newMessages.push(name);
    }
    const currentState = fullTree[treeState][name];
    const nextState = currentState !== undefined ? currentState.children[0] : '';
    const nextChildren = nextState !== '' ? Object.keys(fullTree[nextState]) : [];
    this.setState({
      messages: newMessages,
      currentStateKey: nextState,
      currentState: currentState,
      nextState,
      nextChildren: nextChildren,
      showMessage: false,
    });
  };

  onSubmit = () => {
    this.setState({
      showMessage: true,
      currentStateKey: 'end'
    });
  }

  onClose = () => {
    const { messages } = { ...this.state };
    messages.splice(0, messages.length);
    this.setState({
      ...initialState,
      messages: messages,
      relevantData: true,
      showInfo: false,
    });
  }

  showInfo = () => {
    this.setState({
      showInfo: true,
    });
  }

  renderNodes() {
    const compMap = COMPONENT_MAPPING[this.state.currentStateKey];
    const type = (compMap === undefined) ? 'end' : compMap.type;
    let items = this.state.nextChildren;
    if (this.state.showMessage) {
      items = this.state.messages;
    }
    else if (type === 'body') {
      return <BodySelect
        part={this.state.currentStateKey}
        name={this.state.currentStateKey}
        onClick={this.onNodeClick}
      />
    }
    else if (type === 'quantity') {
      return <Slider
        radius={ 140 }
        border={ 140 }
        value={0.7}
        onChange={(value) => console.log(value)}
      />
    }
    return items.map((key) => (
      <Node
        name={key}
        onClick={this.onNodeClick}
        treeState={this.state.currentStateKey}
      />
    ));
  }

  render() {
    const firstScreen = this.state.currentStateKey === 'person';
    const endScreen = this.state.currentStateKey === 'end';
    let style = (firstScreen || endScreen) ? {
      flexDirection: 'column'
    } : {
      flexDirection: 'row',
      width: '80%'
    };
    if (endScreen) {
      style ={
        flexDirection: 'column',
        width: '20vw',
      }
    }
    const nav = this.state.showInfo ? (
      <span>
        <NavButton name='previous' icon={BACK_ICON} size={40} onClick={this.onClose} />
        <span className='NavText'>RELEVANT DATA</span>
      </span>
    ) : firstScreen ? (
      <NavButton
        name='info'
        icon={INFO_ICON}
        onClick={this.showInfo}
        attention={this.state.relevantData}
      />
    ) : (
      <NavButton name='close' icon={CLOSE_ICON} onClick={this.onClose} />
    );
    return (
      <div className="App">
        {this.state.showInfo && <RelevantData />}
        <div className='App__nodes' style={style}>
          <div className='App__nav'>
            {nav}
          </div>
          {this.renderNodes()}
          {!firstScreen && <MessageBar
            items={this.state.messages}
            onSubmit={this.onSubmit}
          />}
        </div>
      </div>
    )
  }
}

export default App;
